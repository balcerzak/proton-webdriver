package tests;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static utils.ErrorMessages.CANNOT_DELETE_ALL_FOLDERS;
import static utils.ErrorMessages.CANNOT_DELETE_ALL_LABELS;
import static utils.ErrorMessages.CANNOT_GO_TO;
import static utils.ErrorMessages.EXPECTED_DIALOG_NOT_DISPLAYED;
import static utils.ErrorMessages.EXPECTED_FOLDER_NOT_PRESENT;
import static utils.ErrorMessages.UNEXPECTED_NUMBER_OF_FOLDERS;
import static utils.TestData.EXPECTED_LABELS_SETTINGS_URL;
import static utils.TestData.NAME_1;
import static utils.TestData.NAME_2;
import static utils.TestData.NAME_3;
import static utils.TestData.NAME_4;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import pages.LoginPage;
import pages.folderslabels.AddEditElementDialog;
import pages.folderslabels.FoldersLabelsPage;

public class FoldersSettings {

    protected static WebDriver driver;

    @Before
    public void prepareForTest() throws InterruptedException {

        FoldersLabelsPage foldersLabelsPage = new LoginPage(driver)
            .open()
            .loginAs(System.getProperty("user"), System.getProperty("password"))
            .closeOnBoardingDialog()
            .openSettingsPage()
            .openFoldersLabelsPage();

        assertThat(CANNOT_GO_TO + EXPECTED_LABELS_SETTINGS_URL, driver.getCurrentUrl(), is(EXPECTED_LABELS_SETTINGS_URL));
        Thread.sleep(1000);
        foldersLabelsPage.deleteAllFolders();
        assertThat(CANNOT_DELETE_ALL_FOLDERS, foldersLabelsPage.folders().size(), is(0));
        foldersLabelsPage.deleteAllLabels();
        assertThat(CANNOT_DELETE_ALL_LABELS, foldersLabelsPage.labels().size(), is(0));

    }

    @After
    public void shutdown() {
        driver.quit();
    }

    @Before
    public void connectToLocalBrowser() {

        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("disable-notifications");
        options.addArguments("incognito");
        driver = new ChromeDriver(options);

    }

    @Test
    public void userCanAddUniqueFolder() {
        // given
        FoldersLabelsPage page = new FoldersLabelsPage(driver);
        // when
        page.addUniqueFolder(NAME_1);
        // then
        assertThat(UNEXPECTED_NUMBER_OF_FOLDERS, page.folders().size(), is(1));
    }

    @Test
    public void userCannotAddNotUniqueFolder() {
        // given
        FoldersLabelsPage page = new FoldersLabelsPage(driver);
        page.addUniqueFolder(NAME_1);
        assertThat(UNEXPECTED_NUMBER_OF_FOLDERS, page.folders().size(), is(1));
        // when
        AddEditElementDialog dialog = page.openAddFolderDialog();
        dialog.setNameTo(NAME_1).submitButton().click();
        // then
        assertThat(EXPECTED_DIALOG_NOT_DISPLAYED, dialog.isDisplayed(), is(true));
        assertThat(UNEXPECTED_NUMBER_OF_FOLDERS, page.folders().size(), is(1));
    }

    @Test
    public void userCannotHaveMoreThanThreeUniqueFolders() {
        // given
        FoldersLabelsPage page = new FoldersLabelsPage(driver);
        page.addUniqueFolder(NAME_1);
        page.addUniqueFolder(NAME_2);
        page.addUniqueFolder(NAME_3);
        assertThat(UNEXPECTED_NUMBER_OF_FOLDERS, page.folders().size(), is(3));
        // when
        AddEditElementDialog dialog = page.openAddFolderDialog();
        dialog.setNameTo(NAME_4).submitButton().click();
        // then
        assertThat(EXPECTED_DIALOG_NOT_DISPLAYED, dialog.isDisplayed(), is(true));
        assertThat(UNEXPECTED_NUMBER_OF_FOLDERS, page.folders().size(), is(3));
    }

    @Test
    public void userCanChangeFolderNameToAnotherUniqueName() {
        // given
        FoldersLabelsPage page = new FoldersLabelsPage(driver);
        page.addUniqueFolder(NAME_1);
        assertThat(UNEXPECTED_NUMBER_OF_FOLDERS, page.folders().size(), is(1));
        // when
        page.changeFolderName(NAME_1, NAME_2);
        // then
        assertThat(UNEXPECTED_NUMBER_OF_FOLDERS, page.folders().size(), is(1));
        assertThat(EXPECTED_FOLDER_NOT_PRESENT, page.getFoldersNames(), hasItem(NAME_2));
    }

    @Test
    public void userCannotChangeFolderNameToNotUniqueName() {
        // given
        FoldersLabelsPage page = new FoldersLabelsPage(driver);
        page.addUniqueFolder(NAME_1);
        page.addUniqueFolder(NAME_2);
        assertThat(UNEXPECTED_NUMBER_OF_FOLDERS, page.folders().size(), is(2));
        // when
        AddEditElementDialog dialog = page.getFolder(NAME_1).clickEditToOpenDialog();
        dialog.setNameTo(NAME_2).submitButton().click();
        // then
        assertThat(EXPECTED_DIALOG_NOT_DISPLAYED, dialog.isDisplayed(), is(true));
        assertThat(UNEXPECTED_NUMBER_OF_FOLDERS, page.folders().size(), is(2));
        assertThat(EXPECTED_FOLDER_NOT_PRESENT, page.getFoldersNames(), hasItems(NAME_1, NAME_2));
    }

    @Test
    public void userCanDeleteFolder() {
        // given
        FoldersLabelsPage page = new FoldersLabelsPage(driver);
        page.addUniqueFolder(NAME_1);
        assertThat(UNEXPECTED_NUMBER_OF_FOLDERS, page.folders().size(), is(1));
        // when
        page.deleteFolder(NAME_1);
        // then
        assertThat(UNEXPECTED_NUMBER_OF_FOLDERS, page.folders().size(), is(0));
    }

}
