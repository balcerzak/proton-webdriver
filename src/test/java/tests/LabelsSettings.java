package tests;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static utils.ErrorMessages.CANNOT_DELETE_ALL_FOLDERS;
import static utils.ErrorMessages.CANNOT_DELETE_ALL_LABELS;
import static utils.ErrorMessages.CANNOT_GO_TO;
import static utils.ErrorMessages.EXPECTED_DIALOG_NOT_DISPLAYED;
import static utils.ErrorMessages.EXPECTED_LABEL_NOT_PRESENT;
import static utils.ErrorMessages.UNEXPECTED_NUMBER_OF_LABELS;
import static utils.TestData.EXPECTED_LABELS_SETTINGS_URL;
import static utils.TestData.NAME_1;
import static utils.TestData.NAME_2;
import static utils.TestData.NAME_3;
import static utils.TestData.NAME_4;

import java.net.MalformedURLException;
import java.net.URL;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import pages.LoginPage;
import pages.folderslabels.AddEditElementDialog;
import pages.folderslabels.FoldersLabelsPage;

public class LabelsSettings {

    protected static WebDriver driver;

    @Before
    public void prepareForTest() throws InterruptedException {

        FoldersLabelsPage foldersLabelsPage = new LoginPage(driver)
            .open()
            .loginAs(System.getProperty("user"), System.getProperty("password"))
            .closeOnBoardingDialog()
            .openSettingsPage()
            .openFoldersLabelsPage();

        assertThat(CANNOT_GO_TO + EXPECTED_LABELS_SETTINGS_URL, driver.getCurrentUrl(), is(EXPECTED_LABELS_SETTINGS_URL));
        Thread.sleep(1000);
        foldersLabelsPage.deleteAllFolders();
        assertThat(CANNOT_DELETE_ALL_FOLDERS, foldersLabelsPage.folders().size(), is(0));
        foldersLabelsPage.deleteAllLabels();
        assertThat(CANNOT_DELETE_ALL_LABELS, foldersLabelsPage.labels().size(), is(0));

    }

    @After
    public void shutdown() {
        driver.quit();
    }

    @Before
    public void connectToRemoteBrowser() throws MalformedURLException {

        ChromeOptions options = new ChromeOptions();
        options.addArguments("disable-notifications");
        options.addArguments("incognito");
        driver = new RemoteWebDriver(new URL("http://54.38.243.103:4444/wd/hub"), options);

    }

    @Test
    public void addEditDeleteLabels() {

        FoldersLabelsPage page = new FoldersLabelsPage(driver);

        // USER CAN ADD NEW LABEL
        page.addUniqueLabel(NAME_1);
        assertThat(UNEXPECTED_NUMBER_OF_LABELS, page.labels().size(), is(1));
        assertThat(EXPECTED_LABEL_NOT_PRESENT, page.getLabelNames(), hasItem(NAME_1));

        // USER CAN CHANGE LABEL NAME
        page.changeLabelName(NAME_1, NAME_2);
        assertThat(UNEXPECTED_NUMBER_OF_LABELS, page.labels().size(), is(1));
        assertThat(EXPECTED_LABEL_NOT_PRESENT, page.getLabelNames(), hasItem(NAME_2));

        // USER CAN DELETE LABEL
        page.deleteLabel(NAME_2);
        assertThat(UNEXPECTED_NUMBER_OF_LABELS, page.labels().size(), is(0));

        // USER CAN ADD MORE THAN 3 LABELS
        page.addUniqueLabel(NAME_1);
        page.addUniqueLabel(NAME_2);
        page.addUniqueLabel(NAME_3);
        page.addUniqueLabel(NAME_4);
        assertThat(UNEXPECTED_NUMBER_OF_LABELS, page.labels().size(), is(4));
        assertThat(EXPECTED_LABEL_NOT_PRESENT, page.getLabelNames(), hasItem(NAME_4));

        // USER CANNOT CHANGE LABEL NAME TO NOT UNIQUE ONE
        AddEditElementDialog dialog = page.getLabel(NAME_1).clickEditToOpenDialog();
        dialog.setNameTo(NAME_2).submitButton().click();
        assertThat(EXPECTED_DIALOG_NOT_DISPLAYED, dialog.isDisplayed(), is(true));

    }

}
