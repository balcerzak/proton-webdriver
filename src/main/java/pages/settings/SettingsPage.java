package pages.settings;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import pages.base.BasePage;
import pages.folderslabels.FoldersLabelsPage;

public class SettingsPage extends BasePage {

    private static String URL = "https://beta.protonmail.com/settings/labels";

    // Selectors:

    public static By FOLDERS_LABELS_BUTTON = By.cssSelector(".navigation [href='/settings/labels']");

    // Getters:

    public WebElement foldersLabelsButton() { return driver.findElement(FOLDERS_LABELS_BUTTON); }

    public SettingsPage(final WebDriver driver) {
        super(driver);
    }

    // ACTIONS:

    public SettingsPage open() {
        driver.get(URL);
        return new SettingsPage(driver);
    }

    public FoldersLabelsPage openFoldersLabelsPage() {
        this.foldersLabelsButton().click();
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(FoldersLabelsPage.FOLDERS_LABELS_TABLE));
        return new FoldersLabelsPage(driver);
    }


}
