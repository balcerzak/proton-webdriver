package pages.folderslabels;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Folder extends FolderLabelElement {

    // Selectors

    public static By NOTIFICATION_TOGGLE = By.cssSelector("[data-test-id=\"folders/labels:item-notification-toggle\"");

    // Getters

    public WebElement notificationToggle() {
        return this.parent.findElement(NOTIFICATION_TOGGLE);
    }

    public Folder(final WebDriver driver, final WebElement parent) {
        super(driver, parent);
    }

}
