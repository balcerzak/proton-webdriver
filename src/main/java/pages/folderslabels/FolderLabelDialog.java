package pages.folderslabels;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import pages.base.Dialog;

public class FolderLabelDialog extends Dialog {

    // Selectors

    public static By RESET_BUTTON = By.cssSelector("dialog button[type=\"reset\"]");
    public static By SUBMIT_BUTTON = By.cssSelector("dialog button[type=\"submit\"]");

    // Getters

    public WebElement resetButton() {
        return driver.findElement(RESET_BUTTON);
    }

    public WebElement submitButton() {
        return driver.findElement(SUBMIT_BUTTON);
    }

    public FolderLabelDialog(final WebDriver driver) {
        super(driver);
    }

    // ACTIONS

    public FoldersLabelsPage clickOnSubmitAndClose() {
        safeClickOn(submitButton());
        wait.until(ExpectedConditions.invisibilityOfElementLocated(SUBMIT_BUTTON));
        return new FoldersLabelsPage(driver);
    }

    public FoldersLabelsPage clickOnResetAndClose() {
        safeClickOn(resetButton());
        wait.until(ExpectedConditions.invisibilityOfElementLocated(RESET_BUTTON));
        return new FoldersLabelsPage(driver);
    }

}
