package pages.folderslabels;

import java.util.List;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pages.base.BasePage;

public class FoldersLabelsPage extends BasePage {

    // Selectors

    public static By ADD_FOLDER_BUTTON = By.cssSelector("[data-test-id=\"folders/labels:addFolder\"]");
    public static By ADD_LABEL_BUTTON = By.cssSelector("[data-test-id=\"folders/labels:addLabel\"]");

    public static By FOLDERS_LABELS_TABLE = By.cssSelector(".container-section-sticky");
    public static By FOLDER_ROW = By.cssSelector("[data-test-id=\"folders/labels:item-type:folder\"]");
    public static By LABEL_ROW = By.cssSelector("[data-test-id=\"folders/labels:item-type:label\"]");

    // Getters

    public WebElement folderButton() {
        return driver.findElement(ADD_FOLDER_BUTTON);
    }

    public WebElement addLabelButton() {
        return driver.findElement(ADD_LABEL_BUTTON);
    }

    public List<WebElement> folders() {
        return driver.findElements(FOLDER_ROW);
    }

    public List<WebElement> labels() {
        return driver.findElements(LABEL_ROW);
    }

    public FoldersLabelsPage(final WebDriver driver) {
        super(driver);
    }

    // ACTIONS

    public AddEditElementDialog openAddFolderDialog() {
        safeClickOn(folderButton());
        return new AddEditElementDialog(driver);
    }

    public FoldersLabelsPage addUniqueFolder(final String name) {
        safeClickOn(folderButton());
        return new AddEditElementDialog(driver).setNameTo(name).submitAction();
    }

    public FoldersLabelsPage changeFolderName(final String from, final String to) {
        getFolder(from).clickEditToOpenDialog().setNameTo(to).submitAction();
        return new FoldersLabelsPage(driver);
    }

    public FoldersLabelsPage deleteFolder(final String name) {
        getFolder(name).clickDeleteToOpenDialog().confirmAction();
        return new FoldersLabelsPage(driver);
    }

    public FoldersLabelsPage deleteAllFolders() {
        getFoldersNames().forEach(this::deleteFolder);
        return new FoldersLabelsPage(driver);
    }

    // COMPLEX LABEL ACTIONS

    public AddEditElementDialog openAddLabelDialog() {
        safeClickOn(addLabelButton());
        return new AddEditElementDialog(driver);
    }

    public FoldersLabelsPage addUniqueLabel(final String name) {
        safeClickOn(addLabelButton());
        return new AddEditElementDialog(driver).setNameTo(name).submitAction();
    }

    public FoldersLabelsPage changeLabelName(final String from, final String to) {
        getLabel(from).clickEditToOpenDialog().setNameTo(to).submitAction();
        return new FoldersLabelsPage(driver);
    }

    public FoldersLabelsPage deleteLabel(final String name) {
        getLabel(name).clickDeleteToOpenDialog().confirmAction();
        return new FoldersLabelsPage(driver);
    }

    public FoldersLabelsPage deleteAllLabels() {
        getLabelNames().forEach(this::deleteLabel);
        return new FoldersLabelsPage(driver);
    }

    // UTILS

    private WebElement findFolder(final String name) {
        return folders().stream()
            .filter(f -> new Folder(driver, f).name().getText().equals(name))
            .findFirst()
            .get();
    }

    public Folder getFolder(final String name) {
        return new Folder(driver, findFolder(name));
    }

    public List<String> getFoldersNames() {
        return folders().stream()
            .map(f -> new Folder(driver, f).name().getText())
            .collect(Collectors.toList());
    }

    private WebElement findLabel(final String name) {
        return labels().stream()
            .filter(f -> new Label(driver, f).name().getText().equals(name))
            .findFirst()
            .get();
    }

    public Label getLabel(final String name) {
        return new Label(driver, findLabel(name));
    }

    public List<String> getLabelNames() {
        return labels().stream()
            .map(f -> new Label(driver, f).name().getText())
            .collect(Collectors.toList());
    }

}
