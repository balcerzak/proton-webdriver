package pages.folderslabels;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pages.base.BasePage;

public class FolderLabelElement extends BasePage {

    protected WebElement parent;

    // Selectors

    public static By NAME = By.cssSelector("[data-test-id=\"folders/labels:item-name\"]");
    public static By EDIT_BUTTON = By.cssSelector("[data-test-id=\"folders/labels:item-edit\"]");
    public static By DROPDOWN_BUTTON = By.cssSelector("[data-test-id=\"dropdown:open\"]");
    public static By DROPDOWN_DELETE_BUTTON = By.cssSelector(".dropDown[aria-hidden=\"false\"] [data-test-id=\"folders/labels:item-delete\"]");

    // Getters

    public WebElement name() {
        return parent.findElement(NAME);
    }

    public WebElement editButton() {
        return parent.findElement(EDIT_BUTTON);
    }

    public WebElement dropDownButton() {
        return parent.findElement(DROPDOWN_BUTTON);
    }

    public WebElement dropDownDeleteButton() {
        return driver.findElement(DROPDOWN_DELETE_BUTTON);
    }

    public FolderLabelElement(final WebDriver driver, final WebElement parent) {
        super(driver);
        this.parent = parent;
    }

    // ACTIONS

    public AddEditElementDialog clickEditToOpenDialog() {
        safeClickOn(editButton());
        return new AddEditElementDialog(driver);
    }

    public DeleteElementDialog clickDeleteToOpenDialog() {
        safeClickOn(dropDownButton());
        safeClickOn(dropDownDeleteButton());
        return new DeleteElementDialog(driver);
    }

}
