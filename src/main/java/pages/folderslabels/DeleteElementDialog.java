package pages.folderslabels;

import org.openqa.selenium.WebDriver;

public class DeleteElementDialog extends FolderLabelDialog {

    public DeleteElementDialog(final WebDriver driver) {
        super(driver);
    }

    // ACTIONS

    public FoldersLabelsPage confirmAction() {
        return clickOnSubmitAndClose();
    }

    public FoldersLabelsPage cancelAction() {
        return clickOnResetAndClose();
    }

}
