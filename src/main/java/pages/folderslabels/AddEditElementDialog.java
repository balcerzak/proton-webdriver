package pages.folderslabels;

import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AddEditElementDialog extends FolderLabelDialog {

    // Selectors

    public static By NAME_INPUT = By.cssSelector("dialog #accountName");
    public static By COLOR_ICON = By.cssSelector("input[name=\"paletteColor\"]");

    // Getters

    public WebElement nameInput() {
        return driver.findElement(NAME_INPUT);
    }

    public List<WebElement> colorIcons() {
        return driver.findElements(COLOR_ICON);
    }

    public AddEditElementDialog(final WebDriver driver) {
        super(driver);
    }

    // ACTIONS

    private void clearDirtyInput(final WebElement input) {
        for(byte b : input.getAttribute("value").getBytes()) input.sendKeys(Keys.BACK_SPACE);
    }

    public AddEditElementDialog setNameTo(final String name) {
        clearDirtyInput(nameInput());
        nameInput().sendKeys(name);
        return new AddEditElementDialog(driver);
    }

    public AddEditElementDialog setRandomColor() {
        Random rand = new Random();
        safeClickOn(colorIcons().get(rand.nextInt(colorIcons().size())));
        return new AddEditElementDialog(driver);
    }

    public FoldersLabelsPage submitAction() {
        return clickOnSubmitAndClose();
    }

    public FoldersLabelsPage cancelAction() {
        return clickOnResetAndClose();
    }

}
