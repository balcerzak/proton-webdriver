package pages.home;

import org.openqa.selenium.WebDriver;
import pages.base.BasePage;

public class HomePage extends BasePage {

    public HomePage(final WebDriver driver) {
        super(driver);
    }

    // ACTIONS:

    public HomePage closeOnBoardingDialog() {
        new OnBoardingDialog(driver).close();
        return new HomePage(driver);
    }

}
