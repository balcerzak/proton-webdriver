package pages.base;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import pages.settings.SettingsPage;

public class BasePage extends Page {

    // Selectors

    public static By SETTINGS_BUTTON = By.cssSelector("#tour-settings");

    // Getters

    public WebElement settingsButton() {
        return driver.findElement(SETTINGS_BUTTON);
    }

    public BasePage(final WebDriver driver) {
        super(driver);
    }

    // ACTIONS

    public SettingsPage openSettingsPage() {
        this.settingsButton().click();
        wait.until(ExpectedConditions.presenceOfElementLocated(SettingsPage.FOLDERS_LABELS_BUTTON));
        return new SettingsPage(driver);
    }

}
