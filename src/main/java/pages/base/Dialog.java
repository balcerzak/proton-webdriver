package pages.base;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class Dialog extends Page {

    // Selectors

    public static By CLOSE_BUTTON = By.cssSelector("dialog .pm-modalClose");

    // Getters

    public WebElement closeButton() {
        return driver.findElement(CLOSE_BUTTON);
    }

    public Dialog(final WebDriver driver) {
        super(driver);
    }

    // ACTIONS

    public void close() {
        safeClickOn(closeButton());
        wait.until(ExpectedConditions.invisibilityOfElementLocated(CLOSE_BUTTON));
    }

    public boolean isDisplayed() {
        return closeButton().isDisplayed();
    }

}
