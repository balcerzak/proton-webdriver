package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import pages.base.Page;
import pages.home.HomePage;

public class LoginPage extends Page {

    private static String URL = "https://beta.protonmail.com/login";

    // Selectors

    public static By USERNAME_INPUT = By.cssSelector("#username");
    public static By PASSWORD_INPUT = By.cssSelector("#password");
    public static By LOGIN_BUTTON = By.cssSelector("#login_btn");

    // Getters

    public WebElement usernameInput() {
        return driver.findElement(USERNAME_INPUT);
    }

    public WebElement passwordInput() {
        return driver.findElement(PASSWORD_INPUT);
    }

    public WebElement loginButton() {
        return driver.findElement(LOGIN_BUTTON);
    }

    public LoginPage(final WebDriver driver) {
        super(driver);
    }

    // ACTIONS

    public LoginPage open() {
        driver.get(URL);
        wait.until(ExpectedConditions.visibilityOf(usernameInput()));
        return new LoginPage(driver);
    }

    public HomePage loginAs(final String username, final String password) {
        usernameInput().sendKeys(username);
        passwordInput().sendKeys(password);
        loginButton().click();
        wait.until(ExpectedConditions.presenceOfElementLocated(HomePage.SETTINGS_BUTTON));
        return new HomePage(driver);
    }

}
