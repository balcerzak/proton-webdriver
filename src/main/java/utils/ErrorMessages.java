package utils;

public class ErrorMessages {

    public static final String CANNOT_DELETE_ALL_FOLDERS = "Cannot successfully delete all folders";
    public static final String CANNOT_DELETE_ALL_LABELS = "Cannot successfully delete all labels";
    public static final String CANNOT_GO_TO = "Cannot successfully go to: ";

    public static final String UNEXPECTED_NUMBER_OF_LABELS = "Unexpected number of labels";
    public static final String EXPECTED_LABEL_NOT_PRESENT = "Expected label name is not present";

    public static final String EXPECTED_DIALOG_NOT_DISPLAYED = "Expected dialog is not displayed";

    public static final String UNEXPECTED_NUMBER_OF_FOLDERS = "Unexpected number of folders";
    public static final String EXPECTED_FOLDER_NOT_PRESENT = "Expected label name is not present";

}
