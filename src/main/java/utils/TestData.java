package utils;

public class TestData {

    public static final String EXPECTED_LABELS_SETTINGS_URL = "https://beta.protonmail.com/settings/labels";

    public static final String NAME_1 = "Name_1";
    public static final String NAME_2 = "Name_2";
    public static final String NAME_3 = "Name_3";
    public static final String NAME_4 = "Name_4";

}
