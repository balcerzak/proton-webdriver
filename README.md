# ProtonMail: Java and WebDriver #

Aim of this project is to create a showcase for web GUI automated tests, based on ProtonMail service (limited to Folders & Labels Settings page).

Challenges:
* asynchronous nature of user actions
* GUI responsive design (Page Object modelling)
* test reliability and efficiency (test cases design)
* test data maintenance (through GUI)

### Tool set ###

* Java 8
* JUnit 4
* Hamcrest Matchers
* Selenium WebDriver + WebDriver Manager https://github.com/bonigarcia/webdrivermanager
* Selenium Grid (remote server: http://54.38.243.103:4444/grid/console)
* Maven

### Project structure: Page Objects ###

Page Object Pattern is used to model UI elements and compose user actions on top of that. Each page has own subset of element locators (flexible to use with ExpectedConditions synchronisation mechanism) and relevant getters to access WebElements directly.

Page Objects location: `src/main/java/pages`

#### Project structure: tests ####

Two approaches are presented here:
* BDD like given-when-then approach, where all test scenarios are short and focues only on a single feature (example: adding new folder) - pros: each action can be tested in isolation, tests are shorter and easier to read; cons: duplication of user actions and test conditions across different scenarios

```
@Test
public void userCannotAddNotUniqueFolder() {
    // given
    FoldersLabelsPage page = new FoldersLabelsPage(driver);
    page.addUniqueFolder("My Folder");
    assertThat(page.folders().size(), is(1));
    // when
    AddEditElementDialog dialog = page.openAddFolderDialog();
    dialog.setNameTo("My Folder").submitButton().click();
    // then
    assertThat(dialog.isDisplayed(), is(true));
    assertThat(page.folders().size(), is(1));
}
```
* end-to-end aproach, where whole functionality (example: labels management) is tested in a single scenario, going through all available the user actions - pros: less duplications, shorter execution time; cons: tests are fragile and dependent

Test scenarios location: `src/test/java/tests`

### Test execution ###

Tests can be run on a local browser or on remote server (Selenium Grid, BrowserStack etc.)

* To run whole set of tests in given class execute following command: `mvn clean -Dtest={className} test`
* To run single test scenario execute:  `mvn clean -Dtest={className}#{methodName} test`

NOTE: user needs to provide valid username/password for ProtonMail account to run existing tests, example `mvn clean -Dtest=FoldersSettings#userCanAddUniqueFolder -Duser='myUsername' -Dpassword='myPassword' test`